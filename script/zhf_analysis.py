#! /usr/bin/env python

#import argparse
import sys
import os
import glob
import ROOT
#
mainFolder = "/nfs/users/gcallea/Zbb_sample/MakeTTre_out"
inputDir = mainFolder+"/data_all/"

#Booking histograms
h_l1pt = ROOT.TH1D("l1pt", "l1 transverse momentum", 20, 10, 400 )
#Defining the selection
nevt = 0
nevt_sel = 0
isMC = False
TDirectoryPath  = "ZHFTreeAlgo/nominal"
#
print("The input directory is "+inputDir)
for filename in os.listdir(inputDir):
    print(filename)
    inFile = ROOT.TFile.Open(inputDir+filename)
    #
    if "data" not in filename:
        isMC=True
    tree = inFile.Get("ZHFTreeAlgo/nominal")
    print "INFO: TotalEvents: "+str(tree.GetEntries())
    #
    #    for ievt in range(0,tree.GetEntries()):
    for ievt in range(0,1000):
        nevt += 1
        tree.GetEntry(ievt)
        lepton0 = ROOT.TLorentzVector()
        lepton1 = ROOT.TLorentzVector()
        Zcand = ROOT.TLorentzVector()
        if tree.nel < 2 and tree.nmuon < 2:
            continue
        if tree.nel + tree.nmuon > 2:
            continue
        if tree.nel == 2:
            lepton0.SetPtEtaPhiM(tree.el_pt[0],tree.el_eta[0],tree.el_phi[0],tree.el_m[0])   
            lepton1.SetPtEtaPhiM(tree.el_pt[1],tree.el_eta[1],tree.el_phi[1],tree.el_m[1])   
        elif tree.nmuon == 2:
            lepton0.SetPtEtaPhiM(tree.muon_pt[0],tree.muon_eta[0],tree.muon_phi[0],tree.muon_m[0])   
            lepton1.SetPtEtaPhiM(tree.muon_pt[1],tree.muon_eta[1],tree.muon_phi[1],tree.muon_m[1])   
        print("Selecting the Z cand")
        Zcand = lepton0+lepton1
        if Zcand.M() < 40: #units in GeV
            continue
        #
        h_l1pt.Fill(lepton0.Pt())
#            
outHistFile = ROOT.TFile.Open("try.root" ,"RECREATE")
h_l1pt.Write()
outHistFile.Close()
