# Boosted ZHF analysis script
This project includes the analysis script for the upcoming boosted Z+HF analysis
It runs over TTrees that include calibrated objects and will perform the basic event selection.
It was written in pyroot and is not very flexible at the moment.
You would need to put the path to your input files in it and run it in the following way:

python  zhf_analysis.py
